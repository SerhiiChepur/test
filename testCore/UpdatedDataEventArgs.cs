﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace testCore
{
	public class UpdatedDataEventArgs : EventArgs
	{
		public IEnumerable<string> RecordIds { get; set; }
		public IEquatable<Guid> RecordGuids
		{
			get 
			{
				if (RecordIds != null)
				{
					if (RecordIds.Any())
					{
						var result = from id in RecordIds select new Guid(id);
						return (IEquatable<Guid>)result;
					}
				}
				return (IEquatable<Guid>)new List<Guid>();
			}

			set
			{
				if (value != null)
				{
					var castResult = from guidVal in (List<Guid>)value select guidVal.ToString(SampleIconsConstants.GuidFormatString);
					RecordIds = castResult;
				}
			}
		}
	}
}

