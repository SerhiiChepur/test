﻿using System;
using System.Net.Http;
using testCore.Interfaces;
using testCore.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using testCore.Enums;

namespace testCore.DataProviders
{
	public class JsonIconsDataProvider : IIconsDataProvider
	{
		#region Fields
		private string _jsonUri;
		private string _imagesUri;
		#endregion

		#region Constructors
		public JsonIconsDataProvider (string jsonUri, string imagesUri)
		{
			VerifyUri(jsonUri, SampleIconsConstants.MsgErrJsonSrcInvalid);
			VerifyUri(imagesUri, SampleIconsConstants.MsgErrDataSrcInvalid);
			JsonUri = jsonUri;
			ImagesUri = imagesUri;
		}
		#endregion

		#region Events
		public event EventHandler<StatusArgs> DataLoading;
		#endregion

		#region Properties
		public string JsonUri 
		{ 
			get 
			{
				return _jsonUri;
			}
			set 
			{
				VerifyUri(value, SampleIconsConstants.MsgErrJsonSrcInvalid);
				_jsonUri = value;
				return;
			} 
		}

		public string ImagesUri
		{
			get
			{
				return _imagesUri;
			}
			set
			{
				VerifyUri(value, SampleIconsConstants.MsgErrDataSrcInvalid); 
				_imagesUri = value;
				return;
			}
		}

		#endregion

		#region Public methods
		public async Task<byte[]> GetImageByName(string imgName, bool retryIfBuffTooSmall = false)
		{			
			byte[] result = null;
			var imgUri = string.Format ("{0}{1}", ImagesUri, imgName);
			ChangeStatus (LoadingStatus.StartLoad);
			var client = new HttpClient();		
			try
			{		
				if(retryIfBuffTooSmall)
				{
					var response = await client.GetAsync(imgUri, HttpCompletionOption.ResponseHeadersRead);
					var contentLength = response.Content.Headers.ContentLength;
					if(contentLength.HasValue)client.MaxResponseContentBufferSize = contentLength.Value;
				}
				var answer = await client.GetByteArrayAsync(imgUri);
				ChangeStatus(LoadingStatus.LoadSuccess);
				return answer;
			}
			catch(Exception e)
			{
				// Should be logged
				if (retryIfBuffTooSmall) {	
					ChangeStatus (LoadingStatus.LoadError);
				} 
				else {
					return await GetImageByName (imgName, true);
				}
			}
			return result;
		}			

		public async Task<IDictionary<string, byte[]>> GetImageByName(IEnumerable<string> imgName)
		{
			try
			{
				var result = new Dictionary<string, byte[]> ();			
				foreach(var currentName in imgName)
				{
					var img = await GetImageByName(currentName);
					if (img!=null)
					{
						result.Add(currentName, img);
					}
				}
			return result;
			}
			catch(Exception e) 
			{
				//Should be logged
			}
			return null;
		}

		public async Task<IEnumerable<IIconsDataProviderItem>> GetDataAsync()
		{
			ChangeStatus (LoadingStatus.StartLoad);
			var result = new List<IconDataProviderItem> ();
			try
			{
				var client = new HttpClient ();
				var answer = await client.GetStringAsync(JsonUri);
				result = JsonConvert.DeserializeObject<List<IconDataProviderItem>> (answer);
				ChangeStatus (LoadingStatus.LoadSuccess);
			}
			catch (Exception e) 
			{
				// Should be logged
				ChangeStatus (LoadingStatus.LoadError);
			}
			return result;
		}

		public void ChangeStatus (LoadingStatus status)
		{
			if (DataLoading != null) 
			{
				DataLoading (this, new StatusArgs (status));
			}
		}
		#endregion

		#region Private methods
		private void VerifyUri(string uri, string ExceptionMsg)
		{
			Uri result;
			if (!Uri.TryCreate(uri, UriKind.Absolute, out result))
			{
				throw new ArgumentException(ExceptionMsg);
			}
		}
		#endregion

		#region Unimplemented
		public IEnumerable<IIconsDataProviderItem> GetData()
		{
			throw new NotImplementedException();
		}
		#endregion
	}
}

