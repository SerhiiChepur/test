﻿using System;

namespace testCore
{
	public static class SampleIconsConstants
	{
		public const string ContentSourceURI = "https://dl.dropboxusercontent.com/u/44089925/testapp/icons.json";
		public const string ImagesSourceURI = "https://dl.dropboxusercontent.com/u/44089925/testapp/images/";
		public const string CellID = "SampleViewCell";

		public const string DefaultImgKeyName = "DefaultImgKeyNameDefaultImgKeyName";
		public const string GuidFormatString = "D";
		public const string CellSelectedSegueId = "showDetail";

		// UI or MSGs
		public const string MsgErrProviderNull = "Data provider cannot be null";
		public const string MsgErrIncorrectDefImg = "Default image cannot be null or zero-length";
		public const string MsgErrEmptyRecordLst = "Record ID-s list cannot be null or empty";
		public const string MsgErrJsonSrcInvalid = "Invalid Json-file URI";
		public const string MsgErrDataSrcInvalid = "Invalid Images-dir URI";

		public const string UIPortraitHomeButtonTitle = "Master";
}
}