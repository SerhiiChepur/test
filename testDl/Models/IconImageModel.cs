﻿using Realms;
using testDl.Interfaces;

namespace testDl.Models
{
	public class IconImageModel : RealmObject, ISampleIconAppImage
	{
		[ObjectId]
		[Indexed]
		public string ImageName { get; set;}

		public string ImageBytes { get; set; }
	}
}

