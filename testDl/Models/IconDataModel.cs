﻿using System;
using Realms;
using testDl.Interfaces;
using testCore;

namespace testDl.Models
{
	public class IconDataModel :RealmObject, IIconDataObject
	{
		public IconDataModel ()
		{
			Id = Guid.NewGuid().ToString(SampleIconsConstants.GuidFormatString);
		}

		[Indexed]
		public string Id{ get; set;}

		public string Name {get; set;}
		public string Description {get; set;}
		//Images byte arrays converted to string
		public string ThumbnailName {get; set;}
		public string ImageName {get; set;}

		public IconImageModel Thumbnail { get; set;	}

		public IconImageModel Image { get; set;	}
	}
}

