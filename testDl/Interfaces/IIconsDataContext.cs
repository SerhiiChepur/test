﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using testCore;
using testCore.Enums;

namespace testDl.Interfaces
{
	public interface IIconsDataContext
	{
		event EventHandler<UpdatedDataEventArgs> DataChanged;
		event EventHandler<StatusArgs> DataLoading;
		IEnumerable <IIconDataModel> GetData ();
		IEnumerable <IIconDataModel> GetData(string id);
		IEnumerable <IIconDataModel> GetData(IEnumerable<string> id);
		Task<IEnumerable <IIconDataModel>> GetDataAsync();
		Task<IEnumerable<IIconDataModel>> UpdateData(string data, WImgUpdateMode mode);
		Task<IEnumerable<IIconDataModel>> UpdateData(IEnumerable<string> data, WImgUpdateMode mode);
		bool NeedToUpdate(string id);
		bool NeedToUpdate(Guid id);
	}
}

