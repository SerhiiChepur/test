using System;
using UIKit;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using testDl.Interfaces;
using System.Linq;
using testCore.Enums;
using testCore;

namespace test
{
	public partial class DetailViewCustom : UIViewController
	{
		#region Fields
		private UIBarButtonItem portraitHomeButton;
		#endregion

		#region Constructors
		public DetailViewCustom (IntPtr handle) : base (handle)
		{
			
		}
		#endregion

		#region Properties
		public IIosIconViewModel Model { get; set; }
		#endregion

		#region Public methods
		public async override void ViewDidLoad ()
		{			
			base.ViewDidLoad ();

			if (Model != null) 
			{				
				ImageForDisplay.Image = Model.Image;
				if (Application.DataContext.NeedToUpdate(Model.Id))
				{					
					var res = await UpdateImages(Model);
					if (res != null) Model = res;
					ImageForDisplay.Image = Model.Image;
				}
			}
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			if (!UIDevice.CurrentDevice.Name.Contains("Phone"))
			{
				if (InterfaceOrientation == UIInterfaceOrientation.Portrait || InterfaceOrientation == UIInterfaceOrientation.PortraitUpsideDown)
				{
					AddButtonForPortrait();
				}
			}
		}

		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			return true;
		}

		public void AddButtonForPortrait()
		{
			if (!UIDevice.CurrentDevice.Name.Contains("Phone"))
			{
				if (portraitHomeButton == null)
				{
					portraitHomeButton = new UIBarButtonItem();
					portraitHomeButton.Title = SampleIconsConstants.UIPortraitHomeButtonTitle;
					portraitHomeButton.Action = SplitViewController.DisplayModeButtonItem.Action;
				}
			}
			NavigationItem.LeftBarButtonItem = portraitHomeButton;
			NavigationItem.LeftItemsSupplementBackButton = true;
		}

		public void RemoveButtonForPortrait()
		{
			NavigationItem.LeftBarButtonItem = null;
		}
		#endregion

		#region Private methods
		private async Task<IIosIconViewModel> UpdateImages(IIosIconViewModel iosIconViewModel)
		{
			var updated = await Application.DataContext.UpdateData(iosIconViewModel.Id, WImgUpdateMode.BigImg);
			if (updated.Any())
			{
				return GetViews(new List<IIconDataModel> { updated.First() }).First();
			}
			return null;
		}

		private IEnumerable<IIosIconViewModel> GetViews(IEnumerable<IIconDataModel> data)
		{
			var result = new ConcurrentBag<IIosIconViewModel>();
			foreach (var dataItem in data)
			{
				result.Add(new IosIconViewModel(dataItem));
			}
			return result;
		}
		#endregion
	}
}
