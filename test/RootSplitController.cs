using System;
using UIKit;

namespace test
{
	public partial class RootSplitController : UISplitViewController
	{		
		public RootSplitController (IntPtr handle) : base (handle)
		{
			if (UIDevice.CurrentDevice.Name.Contains("Phone"))
			{
				ShouldHideViewController += HideShowMainMenu;
			}
			else 
			{
				Delegate = new CustomSplitViewDelegate();
			}
		}

		public bool HideShowMainMenu (UISplitViewController svc, UIViewController viewController, UIInterfaceOrientation inOrientation)
		{			
			//if(inOrientation == UIInterfaceOrientation.Portrait)
			//	{
			//		return false;
			//	}

			return false;

		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		}
		public override bool ShouldAutorotateToInterfaceOrientation(UIInterfaceOrientation toInterfaceOrientation)
		{
			return true;
		}
	}
}
