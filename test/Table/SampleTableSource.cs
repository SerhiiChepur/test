﻿using System;
using UIKit;
using System.Collections.Generic;
using System.Linq;
using testCore;
using Foundation;

namespace test
{
	public class SampleTableSource : UITableViewSource
	{
		#region Fields
		UITableViewController _controller;
		#endregion

		#region Constructors
		public SampleTableSource(UITableViewController parentController, UITableView table, IEnumerable<IIosIconViewModel> data)
		{			
			_controller = parentController;
			table.RegisterNibForCellReuse (UINib.FromName (SampleIconsConstants.CellID, NSBundle.MainBundle), SampleViewCell.Key);
			Data = new List<IIosIconViewModel> (data);
		}
		#endregion

		#region Properties
		public List<IIosIconViewModel> Data{ get; private set;}
		#endregion

		#region Public methods
		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return Data.Count ();
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{			
			var cell = (SampleViewCell)tableView.DequeueReusableCell (SampleIconsConstants.CellID) ;
			if (cell == null) 
			{
				cell = SampleViewCell.CellCreate ();
			}
			var item = Data [indexPath.Row];
			cell.CellImage.Image = item.Thumbnail;
			cell.CellHeader = item.Name;
			cell.CellContent = item.Description;
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			_controller.PerformSegue (SampleIconsConstants.CellSelectedSegueId, null);
		}
		#endregion
	}
}

