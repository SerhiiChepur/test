﻿using UIKit;
using System.Runtime.InteropServices;
using testDl;
using testDl.Interfaces;
using testCore;
using testCore.DataProviders;
using testCore.Interfaces;
using Foundation;
using System;

namespace test
{
	public class Application
	{
		public static IIconsDataProvider DataProvider { get; set; }
		public static IIconsDataContext DataContext { get; set; }

		// This is the main entry point of the application.
		static void Main (string[] args)
		{
			InitContext();
			// if you want to use a different Application Delegate class from "AppDelegate"
			// you can specify it here.
			UIApplication.Main (args, null, "AppDelegate");
		}

		private static void InitContext()
		{
			var _defaultImage = ToByteArray(UIImage.FromBundle("file.png"));
			DataProvider = new JsonIconsDataProvider(SampleIconsConstants.ContentSourceURI, SampleIconsConstants.ImagesSourceURI);
			DataContext = new IconsDataContext(DataProvider, _defaultImage);
		}

		private static byte[] ToByteArray(UIImage img)
		{
			using (NSData imageData = img.AsPNG())
			{
				byte[] myByteArray = new byte[imageData.Length];
				Marshal.Copy(imageData.Bytes, myByteArray, 0, Convert.ToInt32(imageData.Length));
				return myByteArray;
			}
		}
	}
}
